﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Kerala_Tourisam
{
    [Activity(Label = "Kerala_Tourisam", MainLauncher = true, Icon = "@drawable/icon")]
    public class HomePageActivity : Activity
    {
        

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            
        }
    }
}

