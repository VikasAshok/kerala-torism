﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Model
{
    public class District
    {
        public string districtName { get; set; }
        public int districtImage { get; set; }
    }
}
